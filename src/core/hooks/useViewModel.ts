import { useEffect, useRef } from "react";

export function useViewModel<T>(viewModel: T, owned: boolean = true) {
    const ref = useRef(viewModel);

    useEffect(
        () => () => {
            const current: any = ref.current;
            if (owned && current && typeof current.dispose === "function") {
                current.dispose();
            }
        },
        []
    );

    return ref.current;
}
